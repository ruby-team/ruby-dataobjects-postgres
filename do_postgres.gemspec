#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: do_postgres 0.10.17 ruby lib
# stub: ext/do_postgres/extconf.rb

Gem::Specification.new do |s|
  s.name = "do_postgres".freeze
  s.version = "0.10.17"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Dirkjan Bussink".freeze]
  s.date = "2016-01-24"
  s.description = "Implements the DataObjects API for PostgreSQL".freeze
  s.email = "d.bussink@gmail.com".freeze
  s.extensions = ["ext/do_postgres/extconf.rb".freeze]
  s.extra_rdoc_files = ["ChangeLog.markdown".freeze, "LICENSE".freeze, "README.markdown".freeze]
  s.files = ["ChangeLog.markdown".freeze, "LICENSE".freeze, "README.markdown".freeze, "Rakefile".freeze, "ext/do_postgres/compat.h".freeze, "ext/do_postgres/do_common.c".freeze, "ext/do_postgres/do_common.h".freeze, "ext/do_postgres/do_postgres.c".freeze, "ext/do_postgres/error.h".freeze, "ext/do_postgres/extconf.rb".freeze, "ext/do_postgres/pg_config.h".freeze, "lib/do_postgres.rb".freeze, "lib/do_postgres/encoding.rb".freeze, "lib/do_postgres/transaction.rb".freeze, "lib/do_postgres/version.rb".freeze, "spec/command_spec.rb".freeze, "spec/connection_spec.rb".freeze, "spec/encoding_spec.rb".freeze, "spec/error/sql_error_spec.rb".freeze, "spec/reader_spec.rb".freeze, "spec/result_spec.rb".freeze, "spec/spec_helper.rb".freeze, "spec/typecast/array_spec.rb".freeze, "spec/typecast/bigdecimal_spec.rb".freeze, "spec/typecast/boolean_spec.rb".freeze, "spec/typecast/byte_array_spec.rb".freeze, "spec/typecast/class_spec.rb".freeze, "spec/typecast/date_spec.rb".freeze, "spec/typecast/datetime_spec.rb".freeze, "spec/typecast/float_spec.rb".freeze, "spec/typecast/integer_spec.rb".freeze, "spec/typecast/nil_spec.rb".freeze, "spec/typecast/other_spec.rb".freeze, "spec/typecast/range_spec.rb".freeze, "spec/typecast/string_spec.rb".freeze, "spec/typecast/time_spec.rb".freeze, "tasks/compile.rake".freeze, "tasks/release.rake".freeze, "tasks/retrieve.rake".freeze, "tasks/spec.rake".freeze]
  s.rubyforge_project = "dorb".freeze
  s.rubygems_version = "2.5.2.1".freeze
  s.summary = "DataObjects PostgreSQL Driver".freeze
  s.test_files = ["spec/command_spec.rb".freeze, "spec/connection_spec.rb".freeze, "spec/encoding_spec.rb".freeze, "spec/error/sql_error_spec.rb".freeze, "spec/reader_spec.rb".freeze, "spec/result_spec.rb".freeze, "spec/spec_helper.rb".freeze, "spec/typecast/array_spec.rb".freeze, "spec/typecast/bigdecimal_spec.rb".freeze, "spec/typecast/boolean_spec.rb".freeze, "spec/typecast/byte_array_spec.rb".freeze, "spec/typecast/class_spec.rb".freeze, "spec/typecast/date_spec.rb".freeze, "spec/typecast/datetime_spec.rb".freeze, "spec/typecast/float_spec.rb".freeze, "spec/typecast/integer_spec.rb".freeze, "spec/typecast/nil_spec.rb".freeze, "spec/typecast/other_spec.rb".freeze, "spec/typecast/range_spec.rb".freeze, "spec/typecast/string_spec.rb".freeze, "spec/typecast/time_spec.rb".freeze]

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<data_objects>.freeze, ["= 0.10.17"])
      s.add_development_dependency(%q<rake-compiler>.freeze, ["~> 0.7"])
      s.add_development_dependency(%q<rspec>.freeze, ["~> 2.5"])
    else
      s.add_dependency(%q<data_objects>.freeze, ["= 0.10.17"])
      s.add_dependency(%q<rake-compiler>.freeze, ["~> 0.7"])
      s.add_dependency(%q<rspec>.freeze, ["~> 2.5"])
    end
  else
    s.add_dependency(%q<data_objects>.freeze, ["= 0.10.17"])
    s.add_dependency(%q<rake-compiler>.freeze, ["~> 0.7"])
    s.add_dependency(%q<rspec>.freeze, ["~> 2.5"])
  end
end
